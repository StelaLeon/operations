import org.scalatest.{FlatSpec, Matchers}


class OperationReaderSpec extends FlatSpec with Matchers {

  "A Line" should " be read correctly and returned the tuples of x and y" in {
    val reader = new Parser {}

    val xy = reader.readXYLine("3 4")

    xy._1 should be (3)
    xy._2 should be (4)
  }

  "A Line" should " be read correctly and returned under the expected operation" in {
    val reader = new Parser {}

    val xy = reader.readOperation("3 4 5")

    xy.index1 should be (3)
    xy.index2 should be (4)
    xy.value should be (5)
  }


}