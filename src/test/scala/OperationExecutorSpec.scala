import org.scalatest.{FlatSpec, Matchers}


class OperationExecutorSpec extends FlatSpec with Matchers {

  "A list of operations" should " applied accordingly and the result should be the max of the list" in {
    val listOp = Stream(Operation(1,2,100), Operation(2,5,100),Operation(3,4,100))
    val streamSize = 7

    val valueList = Stream.fill[Int](streamSize)(0)

    val executer = new OperationExecutor with OperationExecutorOps {}

    val myMax = executer.execute(valueList,listOp,streamSize).max

    myMax should be (200)
  }

  "Another list of operations" should " applied accordingly and the result should be the max of the list" in {
    val listOp = Stream(Operation(1,2,100), Operation(5,7,233),Operation(3,3,300))
    val streamSize = 7

    val valueList = Stream.fill[Int](streamSize)(0)

    val executer = new OperationExecutor with OperationExecutorOps {}

    val currVals = executer.execute(valueList,listOp, streamSize)
    val myMax = currVals.max

    currVals.size should be (7)
    myMax should be (300)
  }

  "A list should preserve the size" should "after an operation" in{
    val listOp = Stream(Operation(1,2,100))
    val streamSize = 7

    val valueList = Stream.fill[Int](streamSize)(0)

    val executer = new OperationExecutor with OperationExecutorOps{}

    val currVals = executer.execute(valueList,listOp, streamSize)
    currVals.size should be(7)
    val myMax = currVals.max

    myMax should be (100)
  }



  "A list with 10 000 000 elem" should "still  return the value after an operation" in{
    val listOp = Stream(Operation(9999995,9999999,100))
    val streamSize = 10000000

    val valueList = Stream.fill[Int](streamSize)(0)

    val executer = new OperationExecutor with OperationExecutorOps {}
    val myMax = executer.processOperations(valueList,listOp,streamSize)

    myMax should be (100)
  }
}