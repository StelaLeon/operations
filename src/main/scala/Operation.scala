import scala.annotation.tailrec

case class Operation(index1: Int, index2:Int, value: Int)

trait OperationExecutorOps{
  @tailrec
  final def getMaxFromStream(list:Stream[Int], currMax: Int): Int = {
    list match{
      case Stream.Empty => currMax
      case hd #:: tail => getMaxFromStream(tail,List(hd, currMax).max)
    }
  }
}

trait OperationExecutor {

  this: OperationExecutorOps =>

  def execute(valueList: Stream[Int], commands: Stream[Operation], streamSize: Int):Stream[Int] ={
    commands match{
      case Stream.Empty => valueList
      case op #:: tail => execute(executeOperation(valueList,op, streamSize ) ,tail, streamSize)
    }
  }

  def processOperations(valueList: Stream[Int],
                        commands:Stream[Operation],
                        streamSize:Int):Int={
    getMaxFromStream(execute(valueList,commands,streamSize),0)
  }

  def executeOperation(values: Stream[Int],op: Operation, streamSize: Int): Stream[Int]={
    values.slice(0,op.index1-1) #::: values.slice(op.index1-1,op.index2).map{(v:Int)=>{
      v+op.value
    }} #::: values.slice(op.index2, streamSize)
  }
}


trait Parser {
  def readXYLine(line:String) :(Int, Int) = {
    val lineRegex = """(\d+) (\d+)""".r
    val lineRegex(x,y) = line
    (x.toInt,y.toInt)
  }

  def readOperation(line:String): Operation = {
    val lineRegex = """(\d+) (\d+) (\d+)""".r
    line match{
      case lineRegex(a,b,c) => Operation(a.toInt,b.toInt,c.toInt)
    }
  }

}