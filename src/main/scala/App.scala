import scala.annotation.tailrec

object App {
  def main(args: Array[String]) {

    println("Please write the length of the list and the number of operations separated by space")

    val reader = new Reader{}
    val executer = new OperationExecutor with OperationExecutorOps{}

    val (x: Int,y:Int) =reader.readXY(println,scala.io.StdIn.readLine)
    val opList = reader.readOp(y,Stream.empty)(println,scala.io.StdIn.readLine)

    val valueList = Stream.fill[Int](x)(0)
    val myMax = executer.processOperations(valueList,opList, x)

    println(s"my max: ${myMax}")
  }
}

trait Reader extends Parser{
  def readXY(write: (String)=>Unit,read: ()=>String):(Int,Int)={
    val xyLine = read().trim()
    try{
      readXYLine(xyLine)
    } catch{
      case ex: Exception=> {
        write("Please write the length of the list and the number of operations separated by space")
        readXY(write,read)
      }
    }
  }

  def readOp(n:Int, opList: Stream[Operation])
            (write: (String)=>Unit,read: ()=>String) :Stream[Operation]={
    n match{
      case 0 => opList.reverse
      case x => {
        try {
          write("insert operation: <index1> <index2> <value>")
          val line = read().trim()
          val op = readOperation(line)
          readOp(n-1,op #:: opList)(write,read)
        } catch{
          case ex:Exception=> readOp(n,opList)(write,read)
        }
      }
    }
  }
}
